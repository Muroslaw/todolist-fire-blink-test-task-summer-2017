# Project Todolist 

Test task for company Fire Blink (summer 2017)

## Getting Started

### Prerequisites

Node.js
```
https://nodejs.org/en/
```

MySQL

```
http://www.mysql.ru/download/
```
or
```
https://dev.mysql.com/downloads/
```


### Installing

After install MySQL not forget config it.
```
login: root
password: root
```

For install dependencies and start project using simple comand
```
npm start
```
After all in browser go to 
```
localhost:3000
```
## Authors

* **Myroslaw Telychko** - *Initial work* - [Muroslaw](https://bitbucket.org/Muroslaw/)

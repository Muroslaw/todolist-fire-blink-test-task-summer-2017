module.exports = function (sequelize, DataTypes) {
    let Task = sequelize.define('Task', {
        title: DataTypes.STRING,
        complete: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        UserId: {
            type: DataTypes.INTEGER,
            onDelete: 'cascade',
            onUpdate: 'cascade',
            references: {
                model: require('./user')(sequelize, DataTypes),
                key: 'id',
            }
        }
    });
    return Task;
};
'use strict';

let bcrypt = require("bcrypt-nodejs");

module.exports = function (sequelize, DataTypes) {
    let User = sequelize.define('User', {
        login: DataTypes.STRING,
        password: DataTypes.STRING
    }, {
        instanceMethods: {
            generateHash: function (password) {
                return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null)
            }
        }
    });
    return User;
};
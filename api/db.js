const
    dbConfig = require('config').get('Default.dbConfig'),
    sequelize = require('sequelize'),
    mysql = require('mysql2');

module.exports = {
    createConnection: createConnection,
    closeConnection: closeConnection,
    checkConnection: checkConnection
};

// First for all using mysql driver for creating database, because sequelize can`t do this
let db = mysql.createConnection({
    host: dbConfig.host,
    user: dbConfig.username,
    password: dbConfig.password,
});

db.connect(function (err) {
    if (err) throw err;
    db.query(`CREATE DATABASE IF NOT EXISTS ${dbConfig.database}`, function (err) {
        if (err) throw err;
        db.end(function (err) {
            if (err) throw err;
            checkConnection();
        });
    });
});

function checkConnection() {
    let connection = createConnection();
    connection.authenticate()
        .then(function () {
            console.log(`Successful connect to ${dbConfig.database}`);

            syncModels(connection);
        })
        .catch(function (err) {
            console.error('Unable to connect to the database:', err);
            closeConnection(connection);
        });
}

function createConnection() {
    let connection = new sequelize(dbConfig.database, dbConfig.username, dbConfig.password, {
        host: dbConfig.host,
        dialect: dbConfig.dialect,
        logging: dbConfig.logging,

        pool: {
            max: 5,
            min: 0,
            idle: 10000 // The maximum time, in milliseconds, that a connection can be idle before being released
        },
    });
    return connection
}

function closeConnection(connection) {
    connection.close();
}

function syncModels(connection) {
    let User = connection.import('./models/user'),
        Task = connection.import('./models/task');

    connection.sync()
        .then(function () {
            console.log('All models are synchronized.');
            closeConnection(connection);
        })
        .catch(function (err) {
            console.error('Error in models synchronization:', err);
            closeConnection(connection);
        });
}












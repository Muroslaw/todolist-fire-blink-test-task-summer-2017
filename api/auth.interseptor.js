const
    connection = require('./db').createConnection(),
    User = connection.import('./models/user'),
    Task = connection.import('./models/task'),

    bcrypt = require("bcrypt-nodejs"),

    passport = require('passport'),
    LocalStrategy = require('passport-local'),
    JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt,

    passportConfig = require('config').get('Default.passportConfig'),
    localStrategyCfg = passportConfig.localStrategy,
    jwtStrategyCfg = passportConfig.jwtStrategy,
    jwtOptions = {
        jwtFromRequest: ExtractJwt.fromAuthHeader(),
        secretOrKey: jwtStrategyCfg.secretOrKey
    };

// This 2 hardcode lines needed, because sequelize not understand
// hand created userId foreign key in Task model and throw error
// that user and task haven`t relations
User.hasMany(Task);
Task.belongsTo(User);

module.exports = function initPassportStrategies() {

    //----------Passport Local Strategy--------//
    passport.use('local', new LocalStrategy(localStrategyCfg, localStrategy));

    //----------Passport JWT Strategy--------//
    passport.use('jwt', new JwtStrategy(jwtOptions, jwtStrategy));
};

function localStrategy(login, password, done) {
    User.find({
        where: {login: login},
        attributes: ['id', 'login', 'password']
    })
        .then(function (user) {
            if (!user) {
                return done(null, false, {message: 'User with this login does not exist!'});
            }

            if (!bcrypt.compareSync(password, user.password)) {
                return done(null, false, {message: 'Password not correct!'});
            }

            return done(null, user)
        })
        .catch(function (error) {
            console.error(error);
            return done(error)
        });
}


function jwtStrategy(payload, done) {
    User.find({
        where: {login: payload.login},
        attributes: ['id', 'login']
    })
        .then(function (user) {
            if (!user) {
                return done(null, false, {message: 'Token has expired. Please login.'})
            }

            return done(null, user)
        })
        .catch(function (error) {
            console.error(error);
            return done(error)
        })
}


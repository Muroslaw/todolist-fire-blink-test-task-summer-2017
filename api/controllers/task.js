const express = require('express'),
    passport = require('passport'),
    router = express.Router(),
    connection = require('../db').createConnection();

let Task = connection.import('../models/task');

router.post('/task', passport.authenticate('jwt', {session: false}), function (req, res) {
    if (!(req.body.title && req.body.user.id)) {
        res.json({status: false, message: 'Some problems with user id, please login.'})
    } else
        Task.create({
            title: req.body.title,
            UserId: req.body.user.id
        })
            .then(function (task) {
                res.json({
                    status: true,
                    body: task,
                    message: 'Task successful created.'
                })
            })
            .catch(function (error) {
                console.error(error);
                res.status(412).json({
                    status: false,
                    message: 'Internal Server Error.'
                });
            })
});

router.post('/getTasks', passport.authenticate('jwt', {session: false}), function (req, res) {
    if (!req.body.user.id) {
        res.json({status: false, message: 'Some problems with user id, please login.'})
    } else
        Task.findAll({
            where: {userId: req.body.user.id}
        })
            .then(function (tasks) {
                res.json({
                    status: true,
                    body: tasks
                })
            })
            .catch(function (error) {
                console.error(error);
                res.status(412).json({
                    status: false,
                    message: 'Internal Server Error.'
                });
            })
});

router.put('/task', passport.authenticate('jwt', {session: false}), function (req, res) {
    if (!req.body.task.id) {
        sendError('Some problems with task id.', res)
    } else
        Task.find({where: {id: req.body.task.id}})
            .then(function (task) {
                task.updateAttributes({
                    title: req.body.task.title,
                    complete: req.body.task.complete
                }).then(function (task) {
                    res.json({
                        status: true,
                        message: 'Task successful updated.',
                        body: task
                    })
                })
            })
            .catch(function (error) {
                console.error(error);
                res.status(412).json({
                    status: false,
                    message: 'Internal Server Error.'
                });
            });
});

// Using post because i need request body for pass task id
router.post('/deleteTask', passport.authenticate('jwt', {session: false}), function (req, res) {
    if (!req.body.task.id) {
        res.json({status: false, message: 'Need task id'})
    } else {
        Task.find({where: {id: req.body.task.id}})
            .then(function (task) {
                if (task) {
                    task.destroy();
                }
                res.json({
                    status: true, message: 'Task successful deleted.'
                })
            })
            .catch(function (error) {
                console.error(error);
                res.status(412).json({
                    status: false,
                    message: 'Internal Server Error.'
                });
            });
    }
});

module.exports = router;

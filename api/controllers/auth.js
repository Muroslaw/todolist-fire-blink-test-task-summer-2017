const
    express = require('express'),
    router = express.Router(),
    jwt = require('jsonwebtoken'),
    passport = require('passport'),

    connection = require('../db').createConnection(),
    User = connection.import('../models/user'),

    jwtConfig = require('config').get('Default.jwtConfig');

let jwtSecret = jwtConfig.secret;
let jwtOptions = {
    expiresIn: jwtConfig.tokenExpiry.week
};
let jwtPayload = function (user) {
    return {
        id: user.id,
        login: user.login
    }
};


router.post('/registration', function (req, res, next) {
    if (!(req.body.login || req.body.password)) {
        res.json({status: false, message: 'Need login and password parameters.'})
    } else {
        let login = req.body.login,
            password = req.body.password;

        User.find({
            where: {login: login},
        })
            .then(function (user) {
                if (user) {
                    res.json({
                        status: false,
                        message: 'Sorry but user with this login already exist!'
                    })
                } else User.create({login: login})
                    .then(function (user) {
                        let encryptedPassword = user._modelOptions.instanceMethods.generateHash(password);
                        user.updateAttributes({password: encryptedPassword})
                            .then(function () {
                                let token = jwt.sign(jwtPayload(user), jwtSecret, jwtOptions);
                                res.json({
                                    status: true,
                                    message: `User "${user.login}" created!`,
                                    body: {
                                        user: user,
                                        token: 'JWT ' + token
                                    }
                                })
                            })
                    })
            })
            .catch(function (error) {
                console.error(error);
                res.status(412).json({
                    status: false,
                    message: 'Internal Server Error.'
                });
            });
    }
});

router.post('/login', function (req, res, next) {
    passport.authenticate('local', function (err, user, message) {
        if (err) {
            res.status(412).json({err});
        }

        if (!user) {
            console.log(message);
            res.json({status: false, message: message.message});
        } else {
            let token = 'JWT ' + jwt.sign(jwtPayload(user), jwtSecret, jwtOptions);
            res.json({status: true, user, token, message: 'Successful login.',});
        }
    })(req, res, next);
});

module.exports = router;



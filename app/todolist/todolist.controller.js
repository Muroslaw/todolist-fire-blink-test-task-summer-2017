(() => {

    'use strict';

    angular
        .module('app')
        .controller('TodolistController', TodolistController);

    function TodolistController($log, growl, TodolistService, $localStorage) {
        const vm = this;
        vm.tasks = [];

        vm.logOut = logOut;
        vm.saveTask = saveTask;
        vm.deleteTask = deleteTask;
        vm.addTask = addTask;

        activate();

        ////

        function activate() {
            $log.debug('Init TodolistController ...');
            getAllTasks();
        }


        function checkLocalStorage() {
            if (TodolistService.checkLocalStorage()) {
                vm.user = $localStorage.user;
                return true
            } else growl.error("Please login!", {
                onclose: logOut
            })
        }

        function checkAuth(response) {
            if (!response) {
                growl.error("Internal server error!", {
                    onclose: logOut
                })
            }
            if (response === "Unauthorized" || typeof  response === 'string') {
                growl.error(response, {
                    onclose: logOut
                })
            } else return response
        }

        function logOut() {
            TodolistService.logOut();
        }

        function getAllTasks() {
            checkLocalStorage();
            TodolistService.getAllTasks((res) => {
                checkAuth(res);
                if (!res.status) {
                    growl.error(res.message);
                } else {
                    vm.tasks = res.body;
                    growl.success(res.message);
                }
            })
        }

        function saveTask(task) {
            checkLocalStorage();
            TodolistService.saveTask(task, (res) => {
                checkAuth(res);
                if (!res.status) {
                    growl.error(res.message);
                } else {
                    growl.success(res.message);
                }
            })
        }

        function deleteTask(task) {
            checkLocalStorage();
            TodolistService.deleteTask(task, (res) => {
                checkAuth(res);
                if (!res.status) {
                    growl.error(res.message);
                } else {
                    getAllTasks();
                    growl.success(res.message);
                }
            })
        }

        function addTask() {
            checkLocalStorage();
            TodolistService.addTask((res) => {
                checkAuth(res);
                if (!res.status) {
                    growl.error(res.message);
                } else {
                    getAllTasks();
                    growl.success(res.message);
                }
            })
        }
    }
})();
(() => {

    'use strict';

    angular
        .module('app')
        .service('TodolistService', TodolistService);

    function TodolistService(Endpoints, $http, $localStorage, TokenService, $location) {
        this.checkLocalStorage = checkLocalStorage;
        this.getAllTasks = getAllTasks;
        this.saveTask = saveTask;
        this.deleteTask = deleteTask;
        this.addTask = addTask;
        this.logOut = logOut;


        let res = {}; // Response

        function checkLocalStorage() {
            if ($localStorage.user && TokenService.get()) {
                return {status: true}
            } else  return {status: false}
        }

        function getAllTasks(callback) {
            $http({
                method: 'POST',
                url: Endpoints.API_GET_TASKS,
                data: {
                    user: $localStorage.user
                }
            }).then(function successCallback(response) {
                callback(response.data)
            }, function errorCallback(response) {
                callback(response.data)
            });
        }

        function saveTask(task, callback) {
            $http({
                method: 'PUT',
                url: Endpoints.API_TASK,
                data: {
                    task: task
                }
            }).then(function successCallback(response) {
                callback(response.data)
            }, function errorCallback(response) {
                callback(response.data)
            });
        }

        function deleteTask(task, callback) {
            $http({
                method: 'POST',
                url: Endpoints.API_DELETE_TASK,
                data: {
                    task: task
                }
            }).then(function successCallback(response) {
                callback(response.data)
            }, function errorCallback(response) {
                callback(response.data)
            });
        }

        function addTask(callback) {
            $http({
                method: 'POST',
                url: Endpoints.API_TASK,
                data: {
                    title: "New task",
                    user: $localStorage.user
                }
            }).then(function successCallback(response) {
                callback(response.data)
            }, function errorCallback(response) {
                callback(response.data)
            });
        }

        function logOut() {
            $localStorage.user = null;
            TokenService.clean();
            $location.path('/login');
        }
    }
})();

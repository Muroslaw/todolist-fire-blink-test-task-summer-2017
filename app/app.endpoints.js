(function () {

    'use strict';

    angular
        .module('app')
        .constant('Endpoints', Endpoints());

    function Endpoints() {
        const constants = {
            API: '/api',
            AUTH: '/auth'
        };

        // TASKS
        constants.API_TASK = constants.API + '/task';
        constants.API_GET_TASKS = constants.API + '/getTasks';
        constants.API_DELETE_TASK = constants.API + '/deleteTask';

        // AUTH
        constants.AUTH_LOGIN = constants.AUTH + '/login';
        constants.AUTH_REGISTRATION = constants.AUTH + '/registration';

        return constants;
    }

})();
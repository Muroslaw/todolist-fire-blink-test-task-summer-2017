

(() => {

    'use strict';

    angular
        .module('app')
        .config(config);

    function config($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "static/templates/home.html"
            })
            .when("/login", {
                templateUrl: "static/templates/login.html"
            })
            .when("/registration", {
                templateUrl: "static/templates/registration.html"
            })
            .when("/todolist", {
                templateUrl: "static/templates/todolist.html"
            });
    }
})();


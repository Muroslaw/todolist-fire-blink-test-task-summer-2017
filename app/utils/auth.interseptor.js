(function () {

    'use strict';

    angular
        .module('app')
        .factory('AuthInterceptor', AuthInterceptor);

    function AuthInterceptor(TokenService) {
        return {
            request: function (request) {
                if (TokenService.get()) {
                    request.headers.Authorization = TokenService.get();
                }
                return request;
            }

        };
    }
})();

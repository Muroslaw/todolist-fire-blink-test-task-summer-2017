(() => {

    'use strict';

    angular
        .module('app', [
            'ngRoute',
            'ngResource',
            'ngAnimate',
            'angular-growl',
            'ngStorage',
            'ui.bootstrap'
        ]).run(['$log', ($log) => {
            $log.debug('App is running...');
        }]);
})();
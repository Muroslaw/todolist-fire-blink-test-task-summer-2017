(() => {

    'use strict';

    angular
        .module('app')
        .controller('LoginController', LoginController);

    function LoginController($log, $http, $localStorage, growl, TokenService, $location, Endpoints) {
        const vm = this;

        vm.user = {
            login: null,
            password: null
        };

        vm.login = login;

        activate();

        ////

        function activate() {
            $log.debug('Init LoginController ...');
            checkAuthorization();
        }

        function login() {
            $http({
                method: 'POST',
                url: Endpoints.AUTH_LOGIN,
                data: vm.user
            }).then(function successCallback(response) {
                if (response.data.status) {
                    $localStorage.user = response.data.user;
                    TokenService.save(response.data.token);

                    growl.success(response.data.message, {
                        onclose: function () {
                            $location.path('/todolist');
                        }
                    });
                } else {
                    console.log(response);
                    growl.error(response.data.message)
                }

            }, function errorCallback(response) {
                console.error(response);
            });
        }

        function checkAuthorization() {
            if ($localStorage.user && TokenService.get()) {
                $location.path('/todolist');
            }
        }

    }
})();
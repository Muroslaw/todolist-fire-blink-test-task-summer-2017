(() => {

    'use strict';

    angular
        .module('app')
        .controller('RegistrationController', RegistrationController);

    function RegistrationController($log, $http, $localStorage, growl, TokenService, $location, Endpoints) {
        const vm = this;

        vm.user = {
            login: null,
            password: null
        };

        vm.reg = registration;

        activate();

        ////

        function activate() {
            $log.debug('Init RegistrationController ...');
        }

        function registration() {
            $http({
                method: 'POST',
                url: Endpoints.AUTH_REGISTRATION,
                data: vm.user
            }).then(function successCallback(response) {
                if (response.data.status) {
                    $localStorage.user = response.data.user;
                    TokenService.save(response.data.token);

                    growl.success("Successful register, please login for finish.", {
                        onclose: function () {
                            $location.path('/login');
                        }
                    });
                } else {
                    growl.error(response.data.message)
                }

            }, function errorCallback(response) {
                console.error(response);
            });
        }
    }

})();
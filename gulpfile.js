"use strict";

const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const clean = require('gulp-clean');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const ngAnnotate = require('gulp-ng-annotate');
const rename = require("gulp-rename");


let autoprefixerOptions = {browsers: ['last 2 versions']};


gulp.task('clean', () => {
    let src = ['app/static/vendor/*/'];

    return gulp
        .src(src, {read: false})
        .pipe(clean())
});

gulp.task('copyTemplates', () => {
    let src = 'app/**/*.pug';
    let dest = 'app/static/vendor/views/.';

    gulp
        .src(src)
        .pipe(rename({dirname: ''}))
        .pipe(gulp.dest(dest))

     src = 'app/**/*.html';
     dest = 'app/static/vendor/templates/.';

    return gulp
        .src(src)
        .pipe(rename({dirname: ''}))
        .pipe(gulp.dest(dest))
});

gulp.task('copyScripts', () => {
    let src = [
        'app/app.js',
        'app/app.config.js',
        'app/app.routes.js',
        'app/**/*.js'
    ];
    let dest = 'app/static/vendor/js/.';

    return gulp
        .src(src)
        .pipe(sourcemaps.init())
        .pipe(rename({dirname: ''}))
        .pipe(babel({presets: ['es2015']}))
        .on('error', (err) => {
            console.log('>>> ERROR', err.message);
            this.emit('end');
        })
        .pipe(ngAnnotate())
        .pipe(concat('app.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(dest))
});

gulp.task('copyDependencies', () => {
    let src = [
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/angular/angular.min.js',
        'bower_components/angular-route/angular-route.min.js',
        'bower_components/angular-resource/angular-resource.min.js',
        'bower_components/angular-animate/angular-animate.js',
        'bower_components/angular-growl-v2/build/angular-growl.min.js',
        'bower_components/ngstorage/ngStorage.min.js',
        'bower_components/angular-bootstrap/ui-bootstrap.min.js',
        'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
    ];

    let dest = 'app/static/vendor/js/.';

    return gulp
        .src(src)
        .pipe(sourcemaps.init())
        .pipe(rename({dirname: ''}))
        .pipe(concat('dep.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(dest))
});

gulp.task('copySASS', () => {
    let src = 'app/**/*.scss';
    let dest = 'app/static/vendor/css/app.components/.';

    return gulp
        .src(src)
        .pipe(rename({dirname: ''}))
        .pipe(gulp.dest(dest))
});

gulp.task('compileSass', () => {
    let src = ['app/static/css/dep.scss'];
    let dest = 'app/static/vendor/css/.';

    gulp.src(src)
        .pipe(sourcemaps.init())
        .pipe(rename({dirname: ''}))
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(dest));

    src = ['app/static/css/app.scss'];
    dest = 'app/static/vendor/css/.';

    return gulp
        .src(src)
        .pipe(sourcemaps.init())
        .pipe(rename({dirname: ''}))
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(dest))
});

gulp.task('copyImages', () => {
    let src = 'app/static/img/*';
    let dest = 'app/static/vendor/img/.';

    return gulp
        .src(src)
        .pipe(gulp.dest(dest))
});

gulp.task('copyFonts', () => {

    gulp.src('bower_components/bootstrap-sass/assets/fonts/bootstrap/*.*')
        .pipe(gulp.dest('app/static/vendor/fonts/bootstrap/.'));

    let src = [
        'bower_components/font-awesome/fonts/*.*',
        'bower_components/bootstrap-sass/assets/fonts/bootstrap/*.*',
        'app/static/fonts/*.*'
    ];

    let dest = 'app/static/vendor/fonts/.';

    return gulp
        .src(src)
        .pipe(rename({dirname: ''}))
        .pipe(gulp.dest(dest))
});

gulp.task('default', gulp.series(
    'clean',
    'copyTemplates',
    'copyScripts',
    'copyDependencies',
    'copySASS',
    'compileSass',
    'copyImages',
    'copyFonts'
));



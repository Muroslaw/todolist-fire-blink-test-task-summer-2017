const express = require('express');
const path = require('path');
const http = require('http');
const favicon = require('express-favicon');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const passport = require('passport');

const apiRoutes = require('./api/controllers/task');
const authRoutes = require('./api/controllers/auth');

const app = express();

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(passport.initialize());

app.set('view engine', 'pug'); // Old name - Jade
app.set('views', path.join(__dirname, 'app/static/vendor/views'));

app.use(favicon(path.join(__dirname, 'app/static/vendor/img/favicon.png')));
app.use('/static', express.static(path.join(__dirname, 'app/static/vendor')));

require('./api/auth.interseptor')();


app.use('/auth', authRoutes);
app.use('/api', apiRoutes);
app.get('/', function (req, res, next) {
    res.sendFile(path.join(__dirname, 'app/static/vendor/templates/layout.html'));
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
